/**
 * @param {string} name
 * @param {number} lon
 * @param {number} lat
 * @return {{geometry: {coordinates: [number, number], type: string}, type: string, properties: {name: string}}}
 */
export const createGeoPoint = ({name, lon, lat}) => ({
    "type": "Feature",
    "geometry": {
        "type": "Point",
        "coordinates": [lat, lon]
    },
    "properties": {
        name,
    }
})
