import {fetchPinballMachineList, fetchSensorsList} from './api'

export const MapBaseOptions = {
    container: 'map',
    style: {
        "version": 8,
        "name": "Raster Tiles",
        "glyphs": "https://smellman.github.io/creating_tiles_with_global_map_support_files/2015/mapbox_vector_tile_demo/demosites/fonts/{fontstack}/{range}.pbf",
        "sprite": "https://smellman.github.io/creating_tiles_with_global_map_support_files/2015/mapbox_vector_tile_demo/demosites/maki-sprites/sprite",
        "sources": {
            "osm": {
                "type": "raster",
                "tiles": [
                    "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    "https://c.tile.openstreetmap.org/{z}/{x}/{y}.png"
                ],
                "tileSize": 256
            }
        },
        "layers": [{
            "id": "osm",
            "type": "raster",
            "source": "osm",
            "paint": {
                "raster-fade-duration": 100
            }
        }]
    }, // stylesheet location
    center: [13.374719, 52.518657], // starting position [lng, lat]
    zoom: 9 // starting zoom
}

export const createPointsCollection = ({id, name, color, center, centerName, fetchMethod}) => ({
    id,
    name,
    color,
    center,
    centerName,
    fetchMethod
})


export const PinballMachinesCollection = createPointsCollection({
        id: 'PinballMachines',
        name: 'Pinball machines',
        color: 'red',
        center: [-122.671411, 45.519441],
        centerName: 'Portland', fetchMethod: fetchPinballMachineList
    }
);
export const SensorsCollection = createPointsCollection({
        id: 'Sensors',
        name: 'Sensors',
        color: 'blue',
        center: [13.374719, 52.518657],
        centerName: 'Berlin',
        fetchMethod: fetchSensorsList
    }
);
export const createCollection = (features) => ({
    type: "geojson",
    data: {
        type: 'FeatureCollection',
        features
    }
})
export const PointsCollections = [
    PinballMachinesCollection,
    SensorsCollection
]