/**
 * @param {boolean} isSuccess
 * @param data
 * @return {{data, isSuccess: boolean}}
 */
export const createBaseApiResult = ({isSuccess, data = []}) => ({
    isSuccess,
    data
})