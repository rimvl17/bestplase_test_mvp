import axios from "axios";
import {createBaseApiResult} from "./api";
import {createGeoPoint} from "../model/geo";

/**
 * @return {Promise<{data: ([], String), isSuccess: boolean}>}
 */
export async function fetchSensorsList() {
    try {
        const result = await axios.get('https://api.opensensemap.org/boxes', {
            params: {
                near: '13.424900,52.507076',
                maxDistance: 200000
            }
        })
        const {data} = result;
        return createBaseApiResult({
            isSuccess: true,
            data: data.map(({
                                currentLocation: {
                                    coordinates: [lat, lon]
                                }, sensors
                            }) => createGeoPoint({
                name: `${sensors.length} ${sensors.length > 1 ? 'sensors' : 'sensor'}`,
                lat: Number(lat),
                lon: Number(lon)
            }))
        })
    } catch (e) {
        return createBaseApiResult({isSuccess: false, data: e.message})
    }
}