import axios from "axios";
import {createBaseApiResult} from "./api";
import {createGeoPoint} from "../model/geo";

/**
 * @return {Promise<{data: ([]|string), isSuccess: boolean}>}
 */
export async function fetchPinballMachineList() {
    try {
        const result = await axios.get('https://pinballmap.com/api/v1/locations.json', {
            params: {
                region: 'Portland'
            }
        })
        const {data: {locations}} = result;
        return createBaseApiResult({
            isSuccess: true,
            data: locations
                .map(({name, lat, lon}) => createGeoPoint({
                    name,
                    lat: Number(lon),
                    lon: Number(lat)
                }))
        })
    } catch (e) {
        return createBaseApiResult({isSuccess: false, data: e.message})
    }
}